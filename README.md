This repository shows an example of a setup that enables SSH to travel over the same port as HTTPS to an SSH compliant server. Note that this setup does not *mask* your trafic as HTTPS, it simply receives the SSH traffic on the port that is normally intended for HTTPS.

This repository was made to go with [a blog post on vadosware.io](https://vadosware.io/post/masquerading-ssh-as-https-with-traefik/), you can read more about the idea and bits and pieces there.

# Prerequisites

- A [Kubernetes][k8s] cluster
- A [`cert-manager`][cert-manager] installation (to process the `Certificate`
- A [`traefik`][traefik] installation (to process the `Certificate`

[kubernetes]: https://kubernetes.io/docs
[cert-manager]: https://cert-manager.io/docs
[traefik]: https://doc.traefik.io/traefik/

# Setup

## Add `secrets/ssh-config.configmap.yaml`

Assuming you have an already-valid SSH public key, add it to a [`ConfigMap`][k8s-docs-configmap] that holds that information and is used by the `sshd` `Deployment` we will be creating. Here's a template:

```yaml
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: ssh-config
  namespace: experiments
data:
  authorized_keys: |
    ssh-rsa AAA...
```

This `ConfigMap` will be used to populate the `/root/.ssh` directory in the [`panubo/sshd`][panubo-sshd] container.

[panubo-sshd]: https://github.com/panubo/docker-sshd

## Tweak the Kubernetes resources

You're going to probably want to tweak the following resources to make this code work with *your* infrastructure:

- `sshd-https.ingressroute.yaml`
- `sshd-tls.certificate.yaml`

As this is repository is not meant to be something you can deploy to production right away, I will leave chart-izing it (or wiring it to use `envsubst`) as an exercise for the reader who is interested in being able to perform the customization via ENV.

## Create the resources

The resources can be deployed by running `make` (feel free to peruse the `Makefile`). You should see output like the following:

```console
$ make
```

## SSH to your address

After everything is up and running,

```console
$ ssh -i ~/path/to/your/id_rsa root@ssh-over-https.experiments.vadosware.io -p 443
```

## Tear down the reosurces

```console
$ make uninstall
```

Make sure to tear down the namespace separately as well if you want to.

[k8s-docs-configmap]: https://kubernetes.io/docs/concepts/configuration/configmap/
[k8s]: https://kubernetes.io/docs
