.PHONY: all install uninstall \
				configmap deployment svc ingress certificate

all:  install

install: ns configmap deployment svc ingress certificate

KUBECTL ?= kubectl

ns:
	$(KUBECTL) apply -f experiments.ns.yaml

configmap:
	$(KUBECTL) apply -f secrets/ssh-authorized-users.configmap.yaml
	$(KUBECTL) apply -f secrets/ssh-root-authorized-keys.configmap.yaml
	$(KUBECTL) apply -f www-html.configmap.yaml

deployment:
	$(KUBECTL) apply -f sshd.deployment.yaml

svc:
	$(KUBECTL) apply -f sshd.svc.yaml

ingress:
	$(KUBECTL) apply -f sshd-https.ingressroutetcp.yaml

certificate:
	$(KUBECTL) apply -f sshd-tls.certificate.yaml

uninstall:
	$(KUBECTL) delete -f sshd-https.ingressroutetcp.yaml
	$(KUBECTL) delete -f sshd-tls.certificate.yaml
	$(KUBECTL) delete -f sshd.svc.yaml
	$(KUBECTL) delete -f sshd.deployment.yaml
	$(KUBECTL) delete -f secrets/ssh-authorized-users.configmap.yaml
	$(KUBECTL) delete -f secrets/ssh-root-authorized-keys.configmap.yaml
